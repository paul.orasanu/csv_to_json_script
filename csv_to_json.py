import json
import csv
import plot_setpoints

### Variables ###
INFILENAME = "setpoints.csv"
OUTFILENAME = "setpoints.json"
LISTNAME = "set_points"
###

infile = open(INFILENAME, mode='r', encoding='UTF-8-sig')   # open input csv file in the right encoding mode

spdata = csv.reader(infile)                             # read csv file and save the data in a list of lists.
header = next(spdata)                                   # save first row as headers and skip to next row for iteration start
splist = []                                             # initialize setpoint list
spdict = {}                                             # initialize json dictionary
spdict[LISTNAME] = splist                               # add setpoint list as a dictionary entry

for row in spdata:
    if row[0]!="":                                      # if index element for the row is not empty, entries on that row apply only to current setpoint
        subdict = {}                                    # initialize setpoint subdictionary for every new setpoint   
        for i in range(len(row)):
            if row[i]!="":                              # loop over non-empty row entries
                if header[i] == "type":                 # "type" entries are saved in setpoint subdictionary as strings
                    subdict[header[i]] = row[i]             
                elif row[i].find('.') == True:          # all other entries are saved in setpoint subdictionary as int or float
                    subdict[header[i]] = float(row[i])      
                else:
                    subdict[header[i]] = int(row[i])
        splist.append(subdict)                          # append setpoint subdictionary to setpoint list
    else:                                               # if index element for the row is empty, entries on that row apply to whole list of setpoints
        for i in range(len(row)):
            if row[i]!="":                              # loop over non-empty row entries
                try:                                    # try to save all entries in dictionary as int or float, save as string if this fails
                    if row[i].find('.') == True:
                        spdict[header[i]] = float(row[i])
                    else:
                        spdict[header[i]] = int(row[i])   
                except:
                    spdict[header[i]] = row[i]

outfile = open(OUTFILENAME, mode='w')                   # create or open output json file in write mode
json.dump(spdict, outfile, indent=4)                    # save dictionary to json file with tab indents
plot_setpoints.PlotSetpoints(spdict)                    # plot pressure & temperature setpoints over time

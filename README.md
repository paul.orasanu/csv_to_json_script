How to use:

    1. Edit CSV file according to needs, feel free to add rows or columns.
        - Notes: Empty entry in "Index" column means that values in that row apply to all setpoints.
    
    2. Open python file and edit input or output file names as needed, and change name of setpoint list as needed. 
    
    3. Run python script, and verify that the produced JSON file is valid (Refer to example JSON file if unsure).
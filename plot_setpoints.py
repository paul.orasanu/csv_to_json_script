import matplotlib.pyplot as plt

def PlotSetpoints(sp):
    y_pres = []
    y_temp = []
    t_list = []

    y_pres.append(0)
    y_temp.append(0)
    t_list.append(0)
    prepres = 0
    pretemp = 0
    n = 0

    for i in sp["set_points"]:          # loop over all setpoints
        if i["type"] == "pressure":
            y_pres.append(i["value"])
            y_pres.append(i["value"])
            y_temp.append(pretemp)      # maintain last temperature value if new setpoint is of type "pressure" 
            y_temp.append(pretemp)
            prepres = i["value"]        # save last pressure value
        elif i["type"] == "temperature":
            y_temp.append(i["value"])
            y_temp.append(i["value"])
            y_pres.append(prepres)      # maintain last pressure value if new setpoint is of type "temperature"
            y_pres.append(prepres)
            pretemp = i["value"]        # save last temperature value
        t_list.append(t_list[n] + i["transient_timeout_s"])     # fill time array by adding transient and hold times to previous array entry
        n += 1
        t_list.append(t_list[n] + i["hold_time_s"])
        n += 1


    plt.subplot(211)
    plt.plot(t_list, y_pres, 'b')
    plt.ylabel("Pressure [kPa]")
    plt.title("Pressure & Temperature Setpoints over Time")
    plt.grid(True)
    plt.subplot(212)
    plt.plot(t_list, y_temp, 'r')
    plt.ylabel("Temperature [degC]")
    plt.xlabel("Time [s]")
    plt.grid(True)
    plt.show()